package com.vikingC.camel.CamelProject.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.ToString;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@ToString
public class Country {
	@XmlElement
	private String name;
	
	@XmlElement
	private Hotel hotel;
	
	@XmlElement
	private OpenWeather openWeather;

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	
	public OpenWeather getOpenWeather() {
		return openWeather;
	}

	public void setOpenWeather(OpenWeather openWeather) {
		this.openWeather = openWeather;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
