
package com.vikingC.camel.CamelProject.dto;

import java.util.List;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "term",
    "moresuggestions",
    "autoSuggestInstance",
    "trackingID",
    "misspellingfallback",
    "suggestions",
    "geocodeFallback"
})
@Generated("jsonschema2pojo")
@XmlAccessorType(XmlAccessType.FIELD)
public class Hotel {

    @JsonProperty("term")
    @XmlElement
    private String term;
    
    @JsonProperty("moresuggestions")
    @XmlElement
    private Integer moresuggestions;
    
    @XmlElement
    @JsonProperty("autoSuggestInstance")
    private Object autoSuggestInstance;
    
    @XmlElement
    @JsonProperty("trackingID")
    private String trackingID;
    
    @XmlElement
    @JsonProperty("misspellingfallback")
    private Boolean misspellingfallback;
    
    @XmlTransient
    @JsonProperty("suggestions")
    private List<Suggestion> suggestions;
    
    @XmlElement
    private Suggestion suggestion;
    
    @XmlElement
    @JsonProperty("geocodeFallback")
    private Boolean geocodeFallback;

    @JsonProperty("term")
    public String getTerm() {
        return term;
    }

    @JsonProperty("term")
    public void setTerm(String term) {
        this.term = term;
    }

    @JsonProperty("moresuggestions")
    public Integer getMoresuggestions() {
        return moresuggestions;
    }

    @JsonProperty("moresuggestions")
    public void setMoresuggestions(Integer moresuggestions) {
        this.moresuggestions = moresuggestions;
    }

    @JsonProperty("autoSuggestInstance")
    public Object getAutoSuggestInstance() {
        return autoSuggestInstance;
    }

    @JsonProperty("autoSuggestInstance")
    public void setAutoSuggestInstance(Object autoSuggestInstance) {
        this.autoSuggestInstance = autoSuggestInstance;
    }

    @JsonProperty("trackingID")
    public String getTrackingID() {
        return trackingID;
    }

    @JsonProperty("trackingID")
    public void setTrackingID(String trackingID) {
        this.trackingID = trackingID;
    }

    @JsonProperty("misspellingfallback")
    public Boolean getMisspellingfallback() {
        return misspellingfallback;
    }

    @JsonProperty("misspellingfallback")
    public void setMisspellingfallback(Boolean misspellingfallback) {
        this.misspellingfallback = misspellingfallback;
    }

    @JsonProperty("suggestions")
    public List<Suggestion> getSuggestions() {
        return suggestions;
    }

    @JsonProperty("suggestions")
    public void setSuggestions(List<Suggestion> suggestions) {
        this.suggestions = suggestions;
    }

    @JsonProperty("geocodeFallback")
    public Boolean getGeocodeFallback() {
        return geocodeFallback;
    }

    @JsonProperty("geocodeFallback")
    public void setGeocodeFallback(Boolean geocodeFallback) {
        this.geocodeFallback = geocodeFallback;
    }

	public Suggestion getSuggestion() {
		return suggestion;
	}

	public void setSuggestion(Suggestion suggestion) {
		this.suggestion = suggestion;
	}

	@Override
	public String toString() {
		return "Hotel [term=" + term + ", moresuggestions=" + moresuggestions + ", autoSuggestInstance="
				+ autoSuggestInstance + ", trackingID=" + trackingID + ", misspellingfallback=" + misspellingfallback
				+ ", suggestions=" + suggestions + ", suggestion=" + suggestion + ", geocodeFallback=" + geocodeFallback
				+ "]";
	}

}
