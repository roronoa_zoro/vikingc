package com.vikingC.camel.CamelProject.dto;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;



public class InputFieldData {

	private String country;
	private String city;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date	date;
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "InputFieldData [country=" + country + ", city=" + city + ", date=" + date + "]";
	}
	
	
}
