
package com.vikingC.camel.CamelProject.dto;

import java.util.List;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "group",
    "entities"
})
@Generated("jsonschema2pojo")
@XmlAccessorType(XmlAccessType.FIELD)
public class Suggestion {

    @JsonProperty("group")
    @XmlElement
    private String group;
    
    @XmlElement
    @JsonProperty("entities")
    private List<Entity> entities;

    @JsonProperty("group")
    public String getGroup() {
        return group;
    }

    @JsonProperty("group")
    public void setGroup(String group) {
        this.group = group;
    }

    @JsonProperty("entities")
    public List<Entity> getEntities() {
        return entities;
    }

    @JsonProperty("entities")
    public void setEntities(List<Entity> entities) {
        this.entities = entities;
    }

	@Override
	public String toString() {
		return "Suggestion [group=" + group + ", entities=" + entities + "]";
	}

}
