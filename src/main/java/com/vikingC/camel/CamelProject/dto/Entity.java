
package com.vikingC.camel.CamelProject.dto;

import javax.annotation.Generated;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "geoId",
    "destinationId",
    "landmarkCityDestinationId",
    "type",
    "redirectPage",
    "latitude",
    "longitude",
    "searchDetail",
    "caption",
    "name"
})
@Generated("jsonschema2pojo")
@XmlAccessorType(XmlAccessType.FIELD)
public class Entity {

    @JsonProperty("geoId")
    @XmlElement
    private String geoId;
    @JsonProperty("destinationId")
    @XmlElement
    private String destinationId;
    @JsonProperty("landmarkCityDestinationId")
    @XmlElement
    private Object landmarkCityDestinationId;
    @JsonProperty("type")
    @XmlElement
    private String type;
    @JsonProperty("redirectPage")
    @XmlElement
    private String redirectPage;
    @JsonProperty("latitude")
    @XmlElement
    private Double latitude;
    @JsonProperty("longitude")
    @XmlElement
    private Double longitude;
    @JsonProperty("searchDetail")
    @XmlElement
    private Object searchDetail;
    @JsonProperty("caption")
    @XmlElement
    private String caption;
    @JsonProperty("name")
    @XmlElement
    private String name;

    @JsonProperty("geoId")
    public String getGeoId() {
        return geoId;
    }

    @JsonProperty("geoId")
    public void setGeoId(String geoId) {
        this.geoId = geoId;
    }

    @JsonProperty("destinationId")
    public String getDestinationId() {
        return destinationId;
    }

    @JsonProperty("destinationId")
    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    @JsonProperty("landmarkCityDestinationId")
    public Object getLandmarkCityDestinationId() {
        return landmarkCityDestinationId;
    }

    @JsonProperty("landmarkCityDestinationId")
    public void setLandmarkCityDestinationId(Object landmarkCityDestinationId) {
        this.landmarkCityDestinationId = landmarkCityDestinationId;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("redirectPage")
    public String getRedirectPage() {
        return redirectPage;
    }

    @JsonProperty("redirectPage")
    public void setRedirectPage(String redirectPage) {
        this.redirectPage = redirectPage;
    }

    @JsonProperty("latitude")
    public Double getLatitude() {
        return latitude;
    }

    @JsonProperty("latitude")
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @JsonProperty("longitude")
    public Double getLongitude() {
        return longitude;
    }

    @JsonProperty("longitude")
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @JsonProperty("searchDetail")
    public Object getSearchDetail() {
        return searchDetail;
    }

    @JsonProperty("searchDetail")
    public void setSearchDetail(Object searchDetail) {
        this.searchDetail = searchDetail;
    }

    @JsonProperty("caption")
    public String getCaption() {
        return caption;
    }

    @JsonProperty("caption")
    public void setCaption(String caption) {
        this.caption = caption;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

	@Override
	public String toString() {
		return "Entity [geoId=" + geoId + ", destinationId=" + destinationId + ", landmarkCityDestinationId="
				+ landmarkCityDestinationId + ", type=" + type + ", redirectPage=" + redirectPage + ", latitude="
				+ latitude + ", longitude=" + longitude + ", searchDetail=" + searchDetail + ", caption=" + caption
				+ ", name=" + name + "]";
	}


}
