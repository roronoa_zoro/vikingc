package com.vikingC.camel.CamelProject.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.vikingC.camel.CamelProject.dto.Country;
import com.vikingC.camel.CamelProject.dto.Hotel;
import com.vikingC.camel.CamelProject.dto.InputFieldData;
import com.vikingC.camel.CamelProject.dto.OpenWeather;
import com.vikingC.camel.CamelProject.service.HotelService;
import com.vikingC.camel.CamelProject.service.WeatherMapService;
import com.vikingC.camel.CamelProject.service.XmlWriteService;

@Controller
public class ManualController {
	Logger log = LoggerFactory.getLogger(ManualController.class);
	@Autowired
	HotelService hotelService;
	@Autowired
	WeatherMapService weatherService;
	@Autowired
	XmlWriteService xmlWriteService;


	@GetMapping("/input-field")
	public String getInfoFromInputField(Model model) {
		model.addAttribute("field", new InputFieldData());
		return "input-field";
	}

	@PostMapping("/display-data")
	public String greetingSubmit(@ModelAttribute InputFieldData field, Model model) {
		
		Hotel hotel = hotelService.getHotels(field.getCity());
		OpenWeather weather = weatherService.getWeatherData(field.getCity());
		model.addAttribute("field", field);
		model.addAttribute("hotel", hotel);
		model.addAttribute("weather", weather);
		return "data-display";
	}
	
	
	@GetMapping("/generating")
	public String generatingFile(@RequestParam String countryName, 
								@RequestParam String city) {
		
		Hotel hotel = hotelService.getHotels(city);
		OpenWeather weather = weatherService.getWeatherData(city);
		Country country = new Country();
		country.setName(countryName);
		country.setHotel(hotel);
		country.setOpenWeather(weather);
		if (country != null) {
			xmlWriteService.writeXmlFile(country);
		}
		
		return "generate-xml-file";
	}
}
