package com.vikingC.camel.CamelProject.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.vikingC.camel.CamelProject.controller.ManualController;
import com.vikingC.camel.CamelProject.dto.Country;
import com.vikingC.camel.CamelProject.service.XmlWriteService;

@Service
public class XmlWriteServiceImpl implements XmlWriteService{
	Logger log = LoggerFactory.getLogger(ManualController.class);
	
	@Override
	public void writeXmlFile(Country country) {
		try {

			SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyHHmmss");
			Date now = new Date();
			String batchId = sdf.format(now);
			JAXBContext obj = JAXBContext.newInstance(Country.class);
			Marshaller marObj = obj.createMarshaller();
			marObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			try {
				String fileName =  country.getName() +"_"+ batchId + ".xml";
				String directoryName = "src/main/resources/static/uploads";
				File directory = new File(directoryName);
			    if (! directory.exists()){
			        directory.mkdir();
			    }
				
				String fileLocation = new File(directoryName).getAbsolutePath() + "\\" + fileName;
				marObj.marshal(country, new FileOutputStream(fileLocation));
			} catch (FileNotFoundException e) {
				log.info("Error autoscheduling : " + e.getMessage());
			}

		} catch (JAXBException e) {
			log.info("Error autoscheduling : " + e.getMessage());
		}
		
	}

}
