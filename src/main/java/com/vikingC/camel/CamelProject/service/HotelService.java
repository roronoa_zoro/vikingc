package com.vikingC.camel.CamelProject.service;

import com.vikingC.camel.CamelProject.dto.Hotel;

public interface HotelService {

	public Hotel getHotels(String city);
}
