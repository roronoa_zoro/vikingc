package com.vikingC.camel.CamelProject.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.vikingC.camel.CamelProject.dto.Entity;
import com.vikingC.camel.CamelProject.dto.Hotel;
import com.vikingC.camel.CamelProject.dto.Suggestion;
import com.vikingC.camel.CamelProject.service.HotelService;

@Service
public class HotelServiceImpl implements HotelService{
Logger log = LoggerFactory.getLogger(HotelServiceImpl.class);
	
	@Value("${rapidapi.hotel.url}")
	String hotelUrl;
	@Value("${rapidapi.key.name}")
	String apiKeyName;
	@Value("${rapidapi.key.value}")
	String apiKeyValue;
	@Value("${rapidapi.host.hotel.name}")
	String hotelHostName;
	@Value("${rapidapi.host.hotel.value}")
	String hotelHostValue;

	RestTemplate restTemplate;

	public HotelServiceImpl(RestTemplateBuilder restTemplateBuilder) {
		restTemplate = restTemplateBuilder.build();
	}

	@Override
	public Hotel getHotels(String city) {
		Hotel hotel = new Hotel();
		
		log.info("Entering Method of getHotels of class HotelApi...");
		
		try {
			log.info("Get data for city: {}", city);
			
			HttpHeaders headers = new HttpHeaders();
			headers.set(apiKeyName, apiKeyValue);
			headers.set(hotelHostName, hotelHostValue);
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

			String uriTemp = UriComponentsBuilder.fromHttpUrl(hotelUrl).queryParam("query", "{query}").encode().toUriString();
			
			Map<String, String> params = new HashMap<String, String>();
			params.put("query", city);

			HttpEntity<Hotel> request = new HttpEntity<Hotel>(headers);

			ResponseEntity<Hotel> hotelEntity = restTemplate.exchange(uriTemp, HttpMethod.GET, request, Hotel.class, params);
			
			hotel = hotelEntity.getBody();
			
			int index = 1;
			Suggestion suggestion = new Suggestion();
			List<Entity> hotelEntities = new ArrayList<Entity>();
			
			
			for (Suggestion s : hotel.getSuggestions()) {
				
				if (s.getGroup().equals("HOTEL_GROUP") && !s.getEntities().isEmpty()) {
					
					for (Entity e : s.getEntities()) {
						if (index>3) {
							break;
						}
						hotelEntities.add(e);
						index++;
					}
					s.getEntities().clear();
					s.setEntities(hotelEntities);
					suggestion = s;
				}
			}
			hotel.setSuggestion(suggestion);
			log.info("Existing Method of getHotels of class HotelApi...");
		} catch (Exception e) {
			log.info("Error While Calling API HotelApi: " + e.getMessage());
		}
		return hotel;
	}
}
