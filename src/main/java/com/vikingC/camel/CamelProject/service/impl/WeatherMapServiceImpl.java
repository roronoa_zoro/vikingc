package com.vikingC.camel.CamelProject.service.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.vikingC.camel.CamelProject.dto.Hotel;
import com.vikingC.camel.CamelProject.dto.OpenWeather;
import com.vikingC.camel.CamelProject.service.WeatherMapService;

@Service
public class WeatherMapServiceImpl implements WeatherMapService {
    Logger log = LoggerFactory.getLogger(WeatherMapServiceImpl.class);

    @Value("${openweatherapi.weather.url}")
    String weatherUrl;
    @Value("${openweatherapi.key.name}")
    String apiKeyName;
    @Value("${openweatherapi.key.value}")
    String apiKeyValue;

    RestTemplate restTemplate;

    public WeatherMapServiceImpl(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }

    @Override
    public OpenWeather getWeatherData(String city) {
        log.info("Entering Method of getWeatherData of class WeatherMap...");

        OpenWeather entity = null;


        try {
            log.info("Get data for city: {}", city);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

            String uriTemp = UriComponentsBuilder.fromHttpUrl(weatherUrl)
                    .queryParam("q", "{city}")
                    .queryParam(apiKeyName, "{apiKeyValue}")
                    .encode().toUriString();

            Map<String, String> params = new HashMap<String, String>();
            params.put("city", city);
            params.put("apiKeyValue", apiKeyValue);

            HttpEntity<Hotel> request = new HttpEntity<Hotel>(headers);
            ResponseEntity<OpenWeather> totalEntity = restTemplate.exchange(uriTemp, HttpMethod.GET, request, OpenWeather.class, params);

            entity = totalEntity.getBody();

            log.info("Existing Method of getWeatherData of class WeatherMap...");
        } catch (Exception e) {
            log.info("Error While Calling API WeatherMap: " + e.getMessage());
            //e.printStackTrace();
        }
        return entity;
    }
}
