package com.vikingC.camel.CamelProject.service;

import com.vikingC.camel.CamelProject.dto.OpenWeather;

public interface WeatherMapService {
    OpenWeather getWeatherData(String city);
}
