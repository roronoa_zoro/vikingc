package com.vikingC.camel.CamelProject.service;

import com.vikingC.camel.CamelProject.dto.Country;

public interface XmlWriteService {
	void writeXmlFile(Country country);
}
