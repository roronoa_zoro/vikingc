package com.vikingC.camel.CamelProject;

import org.apache.camel.CamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.vikingC.camel.CamelProject.camelconfig.CamelConfiguration;

@SpringBootApplication
public class CamelProjectApplication {
	@Autowired
	private CamelContext camelContext;
	
	public static void main(String[] args) {
		SpringApplication.run(CamelProjectApplication.class, args);
	}
	
	@Bean
	public CamelConfiguration fileRouteBuilder() throws Exception {
		CamelConfiguration routeBuilder = new CamelConfiguration();
		camelContext.addRoutes(routeBuilder);
		//camelContext.start();
		//Thread.sleep(5*60*1000);
		return routeBuilder;
	}
}
