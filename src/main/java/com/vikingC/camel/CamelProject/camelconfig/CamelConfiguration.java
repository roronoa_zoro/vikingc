package com.vikingC.camel.CamelProject.camelconfig;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.vikingC.camel.CamelProject.dto.Country;
import com.vikingC.camel.CamelProject.dto.Hotel;
import com.vikingC.camel.CamelProject.dto.OpenWeather;
import com.vikingC.camel.CamelProject.service.HotelService;
import com.vikingC.camel.CamelProject.service.WeatherMapService;
import com.vikingC.camel.CamelProject.service.XmlWriteService;

public class CamelConfiguration extends RouteBuilder{
	@Autowired
	HotelService hotelService;
	@Autowired
	WeatherMapService weatherService;
	@Autowired
	XmlWriteService xmlWriteService;
	@Value("${csv.path}")
	String csv;
	
	@Override
	public void configure() throws Exception {
		errorHandler(defaultErrorHandler()
                .maximumRedeliveries(3)
                .redeliveryDelay(1000)
                .loggingLevel(LoggingLevel.DEBUG)
                .retryAttemptedLogLevel(LoggingLevel.DEBUG));
		
		 from("quartz://currentTimer?cron=0 45 22 ? * *")
	        .log("CSV Data Read and Xml File Generate Start....")
	        .process(new Processor() {
				
				@Override
				public void process(Exchange exchange) throws Exception {
					// TODO Auto-generated method stub
					List<List<String>> records = new ArrayList<>();
					try (BufferedReader br = new BufferedReader(new FileReader(csv))) {
					    String line;
					    String headerLine = br.readLine();
					    while ((line = br.readLine()) != null) {
					        String[] values = line.split(",");
					        records.add(Arrays.asList(values));
					    }
					} catch (Exception e) {
						log.info("Error autoscheduling While CSV File Read: " + e.getMessage());
					}

					for (List<String> line : records) {
						String city = line.get(1);
						String cont = line.get(0);
						Hotel hotel = hotelService.getHotels(city);
						OpenWeather weather = weatherService.getWeatherData(city);
						Country country = new Country();
						country.setName(cont);
						country.setHotel(hotel);
						country.setOpenWeather(weather);
						if (country != null) {
							xmlWriteService.writeXmlFile(country);
						}

					}
					
				}
			})
	        .log("Xml File Generate End...");
		
		
		

		from("timer://fileUploadToFtp?fixedRate=true&delay=10s&period=10s")
        .log("${date:now:yyyy-MM-dd'T'HH:mm:ssZ} - Load complete")
        .setHeader("createDate", simple("${date:now:yyyyMMdd'_'HHmmss}"))
        .pollEnrich("file:src/main/resources/static/uploads?noop=true&delay=2000, 0")
        .choice()
        .when(exchange -> exchange.getIn().getBody() != null)
        .to("ftp://{{ftp.server.username}}@{{ftp.server.ip}}/?password={{ftp.server.password}}") // upload the XML file
        // handle case where no file is found 
        .otherwise()
            .log(" skip, no file found. ")
         .end()
        .log("${date:now:yyyy-MM-dd'T'HH:mm:ssZ} - FTP upload complete. File: ${header.CamelFileName}");
	}
	
	
}
